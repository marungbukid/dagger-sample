package com.technistock.daggersample.part1.car

import dagger.Component

/**
 * Components define from which modules (or other components) dependencies are provided.
 * Dagger 2 uses this interface to generate the accessor class which provides the
 * methods defined in the interface.
 */
@Component
interface CarComponent {

    /**
     * Here we satisfy a dependency
     * that would be injected in our scoped
     * component. In our case, we
     * injected this provided dependencies
     * at injectTo(MainActivity)
     *
     * Notice that we didn't define the body for the
     * function provideCar(), dagger generated the code
     * and provide Car instance.
     */
    fun provideCar(): Car


}