package com.technistock.daggersample.part1.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.technistock.daggersample.part1.car.Car
import com.technistock.daggersample.part1.car.DaggerCarComponent
import com.technistock.daggersample.part2.main.MainActivity

class MainActivity : AppCompatActivity() {

    private var car: Car? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Part 1"
        // We wont be defining setContentView for this sample,
        // Please see logcat for our dagger logs.

        val carComponent = DaggerCarComponent.create()

        car = carComponent.provideCar()

        car?.drive()



        Handler().postDelayed({
            Log.d("Part 1", "Loading part 2")
            startActivity(Intent(this@MainActivity, MainActivity::class.java))
        }, 5000)
    }

}