package com.technistock.daggersample.part3.car

import com.technistock.daggersample.part3.WheelsModule
import com.technistock.daggersample.part3.main.MainActivity
import dagger.Component

/**
 * Now since we have define the needed modules, we can tell our component
 * that we depend on our WheelsModule
 */
@Component(modules = [
    WheelsModule::class
])
interface CarComponent {

    /**
     * Here we are stating that dagger needs to inject dependencies of
     * MainActivity (fields).
     */
    fun inject(into: MainActivity)

}