package com.technistock.daggersample.part3.library

import android.util.Log

class Tires {

    fun inflate() {
        Log.d("Tires", "inflating")
    }

}