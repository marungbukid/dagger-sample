package com.technistock.daggersample.part5

import com.technistock.daggersample.part5.car.DieselEngine
import com.technistock.daggersample.part5.car.Engine
import com.technistock.daggersample.part5.car.PetrolEngine
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * This is the optimized version. We can write this class as
 *
 * @Module
 * public class PetroEngineModule {
 *
 *      @Provides
 *      fun provideEngine(petrolEngine: PetrolEngine): Engine {
 *          return petrolEngine
 *      }
 *
 * }
 *
 * With abstract classes, dagger only creates PetrolEngine and don't
 * create implementation of PetrolEngineModule
 */
@Module
class DieselEngineModule(
    private val horsePower: Int
) {

    @Provides
    fun provideEngine(): Engine {
        return DieselEngine(horsePower)
    }

}
