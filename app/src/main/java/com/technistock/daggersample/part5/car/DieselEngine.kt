package com.technistock.daggersample.part5.car

import android.util.Log
import javax.inject.Inject

class DieselEngine @Inject constructor(private val horsePower: Int): Engine {

    override fun start() {
        Log.d("Diesel", "starting diesel engine with horsepower $horsePower")
    }
}