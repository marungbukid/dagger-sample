package com.technistock.daggersample.part5.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.technistock.daggersample.part5.DieselEngineModule
import com.technistock.daggersample.part5.car.Car
import com.technistock.daggersample.part5.car.DaggerCarComponent
import com.technistock.daggersample.part6.main.MainActivity
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Part 5"

        val horsePower = 1000

        /**
         * This is how we inject runtime
         */
        val carComponent = DaggerCarComponent.builder()
            .dieselEngineModule(DieselEngineModule(horsePower))
            .build()

        carComponent.inject(this)

        car.drive()


        Handler().postDelayed({
            Log.d("Part 5", "Loading part 6")
            startActivity(Intent(this@MainActivity, MainActivity::class.java))
        }, 5000)
    }

}