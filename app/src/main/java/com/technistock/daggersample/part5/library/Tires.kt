package com.technistock.daggersample.part5.library

import android.util.Log

class Tires {

    fun inflate() {
        Log.d("Tires", "inflating")
    }

}