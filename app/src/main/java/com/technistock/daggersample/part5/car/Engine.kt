package com.technistock.daggersample.part5.car

import android.util.Log
import javax.inject.Inject

interface Engine {

    fun start()

}