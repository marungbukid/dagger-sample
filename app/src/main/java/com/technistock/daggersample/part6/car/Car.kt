package com.technistock.daggersample.part6.car

import android.util.Log
import com.technistock.daggersample.part6.library.Wheel
import javax.inject.Inject

/**
 * @Inject annotation means that we are telling dagger to
 * provide/inject the dependency we need (engine: Engine)
 *
 * This is called constructor injection.
 */
class Car @Inject constructor(
    private val engine: Engine,
    private val wheel: Wheel
) {

    fun drive() {
        wheel.check()
        engine.start()
        Log.d(TAG, "Driving with Engine $engine")
        Log.d(TAG, "Wheel $wheel")
    }

    companion object {
        private val TAG = Car::class.java.canonicalName
    }

}


/**
 *
 * In java this will be compiled as
 *
 * public class Car {
 *
 *  private Engine engine;
 *
 *      @Inject
 *      public Car(Engine engine) {
 *          this.engine = engine;
 *      }
 *
 * }
 *
 *
 */