package com.technistock.daggersample.part6.car

import android.util.Log
import javax.inject.Inject

interface Engine {

    fun start()

}