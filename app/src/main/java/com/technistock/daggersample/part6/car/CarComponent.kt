package com.technistock.daggersample.part6.car

import com.technistock.daggersample.part6.PetrolEngineModule
import com.technistock.daggersample.part6.WheelsModule
import com.technistock.daggersample.part6.main.MainActivity
import dagger.BindsInstance
import dagger.Component

/**
 * Now since we have define the needed modules, we can tell our component
 * that we depend on our WheelsModule
 */
@Component(modules = [
    WheelsModule::class,
    PetrolEngineModule::class
])
interface CarComponent {

    /**
     * Here we are stating that dagger needs to inject dependencies of
     * MainActivity (fields).
     */
    fun inject(into: MainActivity)

    @Component.Builder
    interface Builder {

        /**
         * This annotation tells dagger to bind instances of
         * integer to those modules/classes that needs instance
         * of type Int
         */
        @BindsInstance
        fun horsePower(horsePower: Int): Builder

        fun build(): CarComponent

    }

}