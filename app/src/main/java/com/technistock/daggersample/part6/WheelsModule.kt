package com.technistock.daggersample.part6

import com.technistock.daggersample.part6.library.Rims
import com.technistock.daggersample.part6.library.Tires
import com.technistock.daggersample.part6.library.Wheel
import dagger.Module
import dagger.Provides

/**
 * @Module are annotations that annotates a class that contributes to the object graph.
 */
@Module
class WheelsModule {

    /**
     * @Provices annotates a method to become a provider.
     */
    @Provides
    fun provideRims(): Rims {
        return Rims()
    }

    @Provides
    fun provideTires(): Tires {
        /**
         * Imagine we need to do configuration to the library
         */
        val tires = Tires()
        tires.inflate()
        return tires
    }

    /**
     * Since dagger knows how to provide rims and tires, we
     * can now create a method provider that depends on rims and tires.
     */
    @Provides
    fun provideWheels(rims: Rims, tires: Tires): Wheel {
        return Wheel(rims, tires)
    }


    /**
     * Since we can't define methods as `static` methods,
     * we need to need dagger that our companion object are
     * module to make dagger efficient. Since provideTires doesn't
     * depend on any instance variable of the module, we can make
     * those methods as static.
     */
    @Module
    companion object {
//        @Provides
//        fun provideTires(): Tires {
//            /**
//             * Imagine we need to do configuration to the library
//             */
//            val tires = Tires()
//            tires.inflate()
//            return tires
//        }


//        @Provides
//        fun provideRims(): Rims {
//            return Rims()
//        }

//        @Provides
//        fun provideWheels(rims: Rims, tires: Tires): Wheel {
//            return Wheel(rims, tires)
//        }
    }

}