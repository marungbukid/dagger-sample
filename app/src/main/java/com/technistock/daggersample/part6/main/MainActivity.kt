package com.technistock.daggersample.part6.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.technistock.daggersample.part6.car.Car
import com.technistock.daggersample.part6.car.DaggerCarComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Part 6"

        val horsePower = 1000

        val carComponent = DaggerCarComponent.builder()
            .horsePower(horsePower)
            .build()

        carComponent.inject(this)

        car.drive()
    }

}