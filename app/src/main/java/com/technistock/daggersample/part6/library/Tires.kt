package com.technistock.daggersample.part6.library

import android.util.Log

class Tires {

    fun inflate() {
        Log.d("Tires", "inflating")
    }

}