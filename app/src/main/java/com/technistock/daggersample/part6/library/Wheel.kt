package com.technistock.daggersample.part6.library

import android.util.Log

class Wheel (
    private val rims: Rims,
    private val tires: Tires
) {

    fun check() {
        Log.d("Wheel", "Checking wheel")
    }

}