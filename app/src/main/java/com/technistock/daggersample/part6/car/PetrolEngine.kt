package com.technistock.daggersample.part6.car

import android.util.Log
import javax.inject.Inject

class PetrolEngine @Inject constructor(
    private val horsePower: Int
): Engine {

    override fun start() {
        Log.d("PetrolEngine", "starting petrol engine horsepower: $horsePower")
    }
}