package com.technistock.daggersample.part4.car

import android.util.Log
import javax.inject.Inject

class DieselEngine @Inject constructor(): Engine {

    override fun start() {
        Log.d("Diesel", "starting diesel engine")
    }
}