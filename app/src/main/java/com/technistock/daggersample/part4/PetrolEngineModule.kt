package com.technistock.daggersample.part4

import com.technistock.daggersample.part4.car.Engine
import com.technistock.daggersample.part4.car.PetrolEngine
import dagger.Binds
import dagger.Module


/**
 * This is the optimized version. We can write this class as
 *
 * @Module
 * public class PetroEngineModule {
 *
 *      @Provides
 *      fun provideEngine(petrolEngine: PetrolEngine): Engine {
 *          return petrolEngine
 *      }
 *
 * }
 *
 * With abstract classes, dagger only creates PetrolEngine and don't
 * create implementation of PetrolEngineModule
 */
@Module
abstract class PetrolEngineModule {

    @Binds
    abstract fun bindEngine(petrolEngine: PetrolEngine): Engine

}
