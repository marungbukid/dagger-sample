package com.technistock.daggersample.part4.car

import android.util.Log
import javax.inject.Inject

class PetrolEngine @Inject constructor(): Engine {

    override fun start() {
        Log.d("PetrolEngine", "starting petrol engine")
    }
}