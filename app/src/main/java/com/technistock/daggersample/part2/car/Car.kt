package com.technistock.daggersample.part2.car

import android.util.Log
import javax.inject.Inject

/**
 * @Inject annotation means that we are telling dagger to
 * provide/inject the dependency we need (engine: Engine)
 *
 * This is called constructor injection.
 */
class Car @Inject constructor(
    private val engine: Engine
) {

    fun drive() {
        engine.start()
        Log.d(TAG, "Driving with Engine $engine")
    }

    companion object {
        private val TAG = Car::class.java.canonicalName
    }

}


/**
 *
 * In java this will be compiled as
 *
 * public class Car {
 *
 *  private Engine engine;
 *
 *      @Inject
 *      public Car(Engine engine) {
 *          this.engine = engine;
 *      }
 *
 * }
 *
 *
 */