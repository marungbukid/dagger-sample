package com.technistock.daggersample.part2.car

import android.util.Log
import javax.inject.Inject

/**
 * We need @Inject annotation for classes that needs
 * to be injected in other classes.
 *
 * It's a best practice to use constructor injection.
 */
class Engine @Inject constructor() {

    fun start() {
        Log.d("Engine", "started")
    }

}