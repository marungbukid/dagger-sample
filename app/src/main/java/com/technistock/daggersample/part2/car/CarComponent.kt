package com.technistock.daggersample.part2.car

import com.technistock.daggersample.part2.main.MainActivity
import dagger.Component

/**
 * Components define from which modules (or other components) dependencies are provided.
 * Dagger 2 uses this interface to generate the accessor class which provides the
 * methods defined in the interface.
 */
@Component
interface CarComponent {

    /**
     * Here we are stating that dagger needs to inject dependencies of
     * MainActivity (fields).
     */
    fun inject(into: MainActivity)

}