package com.technistock.daggersample.part2.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.technistock.daggersample.part2.car.Car
import com.technistock.daggersample.part2.car.DaggerCarComponent
import com.technistock.daggersample.part3.main.MainActivity
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Part 2"

        val carComponent = DaggerCarComponent.create()

        carComponent.inject(this)

        car.drive()

        Handler().postDelayed({
            Log.d("Part 2", "Loading part 3")
            startActivity(Intent(this@MainActivity, MainActivity::class.java))
        }, 5000)
    }

}